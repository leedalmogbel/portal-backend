const prisma = require("../utils/prisma");
require("dotenv").config();
const moment = require("moment");
const { ip } = require("../utils/helpers");
const url = require('url');
const createError = require("http-errors");

class eventService {
  static async create(data) {
    console.log(data)
    try {
      data = {
        ...data,
        seasonId: parseInt(data.seasonId) || 1,
        userId: parseInt(data.userId),
        organizerId: parseInt(data.organizerId) || 1,
        startDate: new Date(data.startDate) || 1,
        endDate: new Date(data.endDate),
        status: data.status || 'approved'
      };
      const event = await prisma.events.create({
        data,
      });

      return event;
    } catch (error) {
      console.log("error", error);
      throw new Error("Contact Administration", 400);
    }
  }

  static async fetchAll(params) {
    try {
      let filter = {};
      if ("id" in params) {
        console.log('asdasd', params)
        if (parseInt(params.id) !== 1) {
          filter = {
            // userId: parseInt(params.id),
            status: 'approved',
            active: true
          };
        }
      }

      if ("active" in params) {
        filter = {
          ...filter,
          active: params.active === "true",
        };
      }

      let count = await prisma.events.count({
        where: {
          ...filter,
        },
      });

      console.log("count", count);

      const allEvents = await prisma.events.findMany({
        where: {
          ...filter,
        },
        include: {
          races: true
        }
      });


      return { data: allEvents, count };
    } catch (error) {
      console.log("error", error);
    }
  }

  static async getEventDetail(id) {
    const event = await prisma.events.findFirst({
      where: {
        id,
      },
      include: {
          races: true
        }
    });

    if (!event) {
      throw createError.NotFound("event not found.");
    }

    return event;
  }

  static async updateStatus(id, status) {
    try {
      const event = await prisma.events.update({
        where: {
          id,
        },
        data: {
          status,
        },
      });

      console.log(id, status)

      return event;
    } catch (error) {
      console.log(error);
      throw new Error("Contact Administration", 400);
    }
  }

  static async updateEvent(id, data) {
    try {

      data = {
        ...data,
        seasonId: parseInt(data.seasonId || 1),
        userId: parseInt(data.userId),
        organizerId: parseInt(data.organizerId || 1),
        startDate: new Date(data.startDate),
        endDate: new Date(data.endDate),
        status: data.status || 'pending'
      };

      const event = await prisma.events.update({
        where: { id: id },
        data: data,
      });

      return event;
    } catch (error) {
      console.log(error);
    }
  }

  static async searchEventByName(id, query) {
    try {
      let filter = {};
      // if (parseInt(id) > 1) {
        filter = {
          // active: true,
          status: 'approved'
        };
      // } 

      let event = [];
      // if (parseInt(id) === 1) {
      //   event = await prisma.$queryRawUnsafe(`SELECT * FROM events where firstName LIKE '%${query}%' OR lastName LIKE '%${query}%'`);
      // } else {
        event = await prisma.$queryRawUnsafe(`SELECT * FROM events where name LIKE '%${query}%' AND status='approved'  `);
      // }
      
      console.log(filter)
      console.log(query)

      return { data: event, count: event.length };
    } catch (error) {
      console.log(error);
    }
  }

  static async fetchEventsForKiosk(params) {
    try {
      let today = moment().format();
      console.log('adasdToday', today)
      let eventDay = moment().add(1, 'days').format();
      console.log('eventDay', eventDay)
      let filter = {};
      let raceFilter = {};
      filter = {
        status: 'approved',
        active: true,
        startDate: {
          gte: today,
          lte: eventDay
        }
      };

      raceFilter = {
        status: 'approved',
        active: true,
        eventDate: {
          gte: today,
          lte: eventDay
        }
      };

      // let count = await prisma.races.count({
      //   where: {
      //     ...filter,
      //   },
      // });

      let allEvents = await prisma.events.findMany({
        where: {
          ...filter,
        },
        select: {
          id: true,
          name: true,
          startDate:true,
          endDate: true,
          races: {
            where: {
              ...raceFilter
            }
          }
        }
      });
      // console.log(allEvents)

      let _races = allEvents.map((event) => {
        let tmpRaces = event?.races;
        let startDate = moment(event.startDate).format('MMMM D, YYYY');
        let endDate = moment(event.endDate).format('MMMM D, YYYY');
        let eventDate = '';
        let startTime = '';

        const races = tmpRaces.map((race) => {
          eventDate = moment(race.eventDate).format('MMMM D, YYYY');
          startTime = moment(race.eventDate).format('hh:mm:ss a');
          const locPath = race.raceImg ? race?.raceImg?.file?.path : {};

          return {
              raceId: race.id,
              raceName: `${race.raceCode} ${race.distance? race.distance:''}, ${race.name}`,
              location: race.location,
              raceImg: `http://${ip}:7331/${url.parse(locPath).href}`,
              eventDate: eventDate,
              startTime: startTime,
          }
        });
        // console.log(moment(eventDate) == moment(endDate))
        console.log(eventDate, endDate)
        console.log(eventDate === endDate)

        return {
            eventId: event.id,
            eventName: event.name,
            eventSD: startDate,
            ...(endDate === eventDate ? {}:{endDate}),
            logo: event.location === 'Bouthib, UAE' ? `http://${ip}:7331/uploads/assets/images/biec_logo_hq.png` : `http://${ip}:7331/uploads/assets/images/eiev_logo_hq.png`,
            eventImg: `http://${ip}:7331/uploads/assets/images/event_0002.jpg`,
            races,
        }
      });

      return { data: _races };
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = eventService;
